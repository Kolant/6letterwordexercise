﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PeripassTest {
    public static class WordCombiner {
    public static List<string> CombinerWords(int wordLength, List<string> words) {
        var result = new List<string>();
        words = words.Distinct().ToList();
        words = words.OrderBy(x => x.Length).ToList();

        for (var i = 0; i < words.Count && words[i].Length < wordLength; i++) {
            for (var j = words.Count - 1; j >= 0; j--) {
                if (i != j && words[i].Length + words[j].Length == wordLength) {
                    var stringBuilder = new StringBuilder(words[i]);
                    stringBuilder.Append($"+{words[j]}");
                    stringBuilder.Append($"={words[i]}");
                    stringBuilder.Append($"{words[j]}");

                    result.Add(stringBuilder.ToString());
                }

                if(words[i].Length + words[j].Length > wordLength) continue;
                if(words[i].Length + words[j].Length < wordLength) break;
            }
        }

        return result;
    }
  }
}
