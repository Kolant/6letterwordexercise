﻿using System.Collections.Generic;

namespace PeripassTest {
    public interface IWordCombiner {
        List<string> CombineWords(int wordLength, List<string> words);
    }
}