﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace PeripassTest
{
    class Program
    {
        static void Main(string[] args)
        {
            var wordLength = 6;
            var inputPath = @"..\..\..\..\..\input.txt";
            var outputPath = @"..\..\..\..\..\output.txt";

            var lines = FileManager.ReadWords(inputPath);
            var combinedResults = WordCombiner.CombinerWords(wordLength, lines);

            FileManager.WriteWords(outputPath, combinedResults);
            foreach(var result in combinedResults)
            {
                Console.WriteLine(result);
            }   
        }
  }
}
