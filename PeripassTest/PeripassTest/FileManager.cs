﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace PeripassTest {
    public static class FileManager
    {
        public static List<string> ReadWords(string path)
        {
            var result = new List<string>();

            if (!File.Exists(path))
            {
                throw new FileNotFoundException();
            }

            result = File.ReadLines(path).ToList();

            return result;
        }

        public static void WriteWords(string path, List<string> lines)
        {
            if(File.Exists(path))
            {
              File.Delete(path);
            }
            else 
            {
              var fileStream = File.Create(path);
              fileStream.Close();
            }

            File.WriteAllLines(path, lines);
        }
  }
}
