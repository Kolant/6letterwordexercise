﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;

namespace PeripassTest.Tests {
  [TestFixture]
  public class WordCombinerTests {
    [SetUp]
    public void Setup() {
    }

    [Test]
    public void CombineWords_InputTxtWordLength6_Success() {
      var wordLength = 6;
      var inputPath = @"..\..\..\..\..\input.txt";
      TestFileWithWordLength(wordLength, inputPath);
    }

    [Test]
    public void CombineWords_Input2TxtWordLength6_Success() {
      var wordLength = 6;
      var inputPath = @"..\..\..\..\..\input2.txt";
      TestFileWithWordLength(wordLength, inputPath);
    }

    private static void TestFileWithWordLength(int wordLength, string inputPath) {
      var words = FileManager.ReadWords(inputPath);
      var bruteForceResult = new List<string>();

      var combinedResults = WordCombiner.CombinerWords(6, words)
        .Select(x => x.Split("=")[1])
        .Distinct()
        .ToList();

      words = words.Distinct().ToList();
      foreach (var word in words) {
        foreach (var word2 in words) {
          if (word != word2 && word.Length + word2.Length == wordLength) {
            bruteForceResult.Add($"{word}{word2}");
          }
        }
      }
      bruteForceResult = bruteForceResult.Distinct().ToList();

      combinedResults.Sort();
      bruteForceResult.Sort();

      Assert.IsTrue(combinedResults.Count == bruteForceResult.Count);
      Assert.AreEqual(bruteForceResult, combinedResults);
      Assert.Pass();
    }
  }
}